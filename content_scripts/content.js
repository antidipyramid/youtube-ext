
const getElementsToRemove = () => browser.runtime.sendMessage({command: "getsaved"});

function hide(option) {
	function toggleElementById(id) {
		try {
			let node = document.getElementById(id);
			console.log("display value of " + id + " is " + node.style.getPropertyValue("display"));			
			if(node.style.getPropertyValue("display") != "none") {
				console.log("toggling " + id + " off");
				node.style.setProperty("display","none");
			} else {
				console.log("toggling " + id + " on");
				node.style.setProperty("display","");
				console.log(node.style.getPropertyValue("display"));
				console.log("display value of " + id + " is now " + node.style.getPropertyValue("display"));
			}
		} catch(e) { console.log(e.message); } //element probably hasn't loaded yet
	}	

	function toggleElementsById(ids) {
		for(let id of ids) {
			toggleElementById(id);
		}	
	}

	function toggleClass(className) {
		let elements = document.getElementsByClassName(className);
	
		for(let element of elements) {
			if(element.style.getPropertyValue("display") != "none") {
				element.style.setProperty("display","none");
			} else {
				element.style.setProperty("display","");
			}
		}
	} 

	console.log("doing stuff");

	switch(option) {
		case Array.isArray(option):
			toggleElementsById(option);
			break;
		case "related": 
			toggleElementById(option);
			break;
		case "comments":
			toggleElementById(option);
			break;
		case "end_recs":
			toggleClass("ytp-endscreen-content");
			break;
		case "home_recs":
			console.log("url is: " + document.URL);
			console.log("reg ex url check: " + /^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)youtube.com$/.test(document.URL));
			toggleElementById("contents");
	}
}


window.addEventListener("transitionend", async function(event) {
	removeSavedOptions();
});

async function removeSavedOptions() {
	let alreadyChecked = await getElementsToRemove();
	console.log("already checked " + alreadyChecked.response);

	if(alreadyChecked.response.length > 0) {	
		console.log(alreadyChecked.response[0])		
		for(let option of alreadyChecked.response) {
			console.log(option);
			hide(option);
		}
	}
}

let videoList = document.getElementsByTagName("video");
let video = videoList[0]

video.addEventListener("ended", event => {
	console.log("hiding endscreen");
	hide("ytp-endscreen-content");
});

let handleMessage = (request,sender,sendResponse) => {
	console.log("Message received: " + request.command);
	hide(request.command);
	sendResponse({tab_url: document.URL, text: request.command});	
}


browser.runtime.onMessage.addListener(handleMessage);

function sendMessageToTabs(tabs,id) {
	for(let tab of tabs) {
		console.log("sending message to tab " + tab.url + " for " + id);
		browser.tabs.sendMessage(tab.id,{command: id})
			.then(response => console.log("Tab URL: " + response.tab_url +
				" " + "Command: " + response.text));
	}
}

function getChecked() {
	return browser.runtime.sendMessage({command: "getsaved"});
}

function check(option) {
	return browser.runtime.sendMessage({command: "checked", option: option});
}

function unCheck(option) {
	return browser.runtime.sendMessage({command: "uncheck", option: option});
}

function onError(error) {
	console.error(`Error: ${error}`);
}

async function applyAlreadySaved() {
	console.log("getting saved settings from bg script");
	let alreadyChecked = await getChecked();

	for(let option of alreadyChecked.response) {
		console.log(option);
		document.getElementById(option).checked = true;
	}
}

document.addEventListener("change", async function(e) {
	let id = e.target.id;
	//let alreadyChecked = await getChecked();

	let tabs = await browser.tabs.query({url:"*://*.youtube.com/*"})

	console.log(e.target.id);

	if(id != undefined) {

		if(e.target.checked === true) {
			check(id);
			sendMessageToTabs(tabs,id);
		}
		else {
			unCheck(id);
			sendMessageToTabs(tabs,id);
		}
	}

	browser.runtime.sendMessage({command: "print"});
});

applyAlreadySaved();

//The background script tracks and saves changes to user preferences

let settings = {
	comments: false,
	related: false,
	end_recs: false,
	home_recs: false,
	
    //channelBlacklist: [], 
    //keywordBlacklist: [] 
};


browser.runtime.onMessage.addListener((message,sender,sendResponse) => {
	switch(message.command) {
		case "getsaved":
			sendResponse({response: Object.keys(settings).filter(s => settings[s])});
			break;	
		case "checked":
			console.log("bg checked for " + message.option + " " + settings[message.option]);
			settings[message.option] = true;
			console.log("bg after check for " + message.option + " " + settings[message.option]);
			break;
		case "uncheck":
			console.log("bg got uncheck for " + message.option + " " + settings[message.option]);
			settings[message.option] = false;		
			console.log("bg after uncheck for " + message.option + " " + settings[message.option]);
			break;
		case "print":
			console.log("comments: " + settings.comments + " related: " + 
				settings.related + "home recs: " + settings.home_recs);
			break;
	}
});
